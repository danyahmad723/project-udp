﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpClient
{
    public static void Main()
    {
        string us, user;
        string masuk;
        byte[] username = new byte[1024];
        byte[] data = new byte[1024];
        string input, stringData;
        IPEndPoint ipep = new IPEndPoint(
                        IPAddress.Parse("127.0.0.1"), 9050);

        Socket server = new Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp);


        string welcome = "connected!";
        data = Encoding.ASCII.GetBytes(welcome);
        server.SendTo(data, data.Length, SocketFlags.None, ipep);

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)sender;

        data = new byte[1024];
        int recv = server.ReceiveFrom(data, ref Remote);

        Console.WriteLine(" input Username : ");
        user = Console.ReadLine();
        server.SendTo(Encoding.ASCII.GetBytes(user), Remote);

        username = new byte[1024];
        int terima = server.ReceiveFrom(username, ref Remote);
        us = Encoding.ASCII.GetString(username, 0, terima);
        Console.WriteLine("username Server :" + Encoding.ASCII.GetString(username, 0, terima));


        while (true)
        {
            Console.Write(user + " : ");
            input = Console.ReadLine();
            if (input == "exit")
                break;
            server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
            data = new byte[1024];
            recv = server.ReceiveFrom(data, ref Remote);
            stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine(user + " : " + stringData);
        }
        Console.WriteLine("Stopping client");
        server.Close();
    }
}